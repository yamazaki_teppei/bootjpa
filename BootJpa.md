# SpringBoot + JPA 開発手順書
## STSのインストール
#### 1.EclipseのヘルプからEclipseマーケットプレースを選択
<br/>

![Alt text](./photo/install1.PNG)

<br/><br/>

#### 2.stsで検索
#### 3.Spring Toolsをインストール
<br/>

![Alt text](./photo/install2.PNG)
<br/><br/><br/><br/>

## 開発環境の準備<br/>
#### 1.ウィンドウ→パースペクティブ→パースペクティブを開く→その他<br/>
<br/>

![Alt text](./photo/window1.PNG)<br/>
<br/>

#### 2.Springを選択して開く
#### 3.パースペクティブにSpringが追加されたのを確認してください
<br/>
![Alt text](./photo/window2.PNG)
<br><br>

## プロジェクトの作成

#### 1.ファイル→新規→その他　から　SpringBootの「Springスターター・プロジェクト」を選択

![Alt text](./photo/project1.PNG)

#### 2.名前、パッケージ名や開発環境を入力　今回は型はGradle 言語はJava バージョンは8にしています

![Alt text](./photo/project3.PNG)
<br/><br/>

#### 3.依存関係の設定
#### Lommbook・JPA・Web・MySQL・Thymeleafを選択
(初めてプロジェクトを作成するときは使用可能の中から選んでください)

![Alt text](./photo/project2.PNG)
#### 4.次へ→完了
プロジェクトが完成したら以下を確認してください
<br>
<br>

##### ・依存関係の確認・追加
build.gradleの以下の部分が依存関係になっています<br>
追加したい場合はこちらに追加してください


```
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	compileOnly 'org.projectlombok:lombok'
	runtimeOnly 'mysql:mysql-connector-java'
	annotationProcessor 'org.projectlombok:lombok'
	testImplementation('org.springframework.boot:spring-boot-starter-test') {
		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	}
```

#### 前準備（MySQLで実行）

データベース作成
```SQL
create database test;
```
テーブルの作成
```SQL
create table test(
id Integer AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) Not null
);
```
テーブルに値を入れる
```SQL
insert into test (id , name) value 
(1 , 'ALHinc'),
(2 , 'ALH'),
(3 , 'あいうえお');

```

<br><br>

##### ・MySQLの設定
src/main/resourcesの中にあるapplication.propertiesnに以下を追加
```xml
spring.datasource.url=jdbc:mysql://localhost/test?characterEncoding=UTF-8&serverTimezone=JST
spring.datasource.username=USERNAME
spring.datasource.password=PASSWORD
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
```
<br><br>




### プログラムの作成
### 1. Entityの作成<br>
User.java

```java
package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Table(name="test") //データベースのテーブル名を指定
@Entity
public class User{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}
```
 <br><br>
### 2. Repositoryの作成
 
　UserRepository.java

```java
package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {}
```
<br><br>
### 3. Controllerの作成
 
　HomeController.java

```java
package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;


@Controller
public class HomeController {


    @Autowired
    UserService userService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayList(Model model) {
        List<User> userlist = userService.searchAll();
        model.addAttribute("userlist", userlist);
        return "index";
    }
}
```
<br><br>

### 4. Serviceの作成
 
　UserService.java

```java
package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;


@Service
@Transactional(rollbackOn = Exception.class)
public class UserService {


    @Autowired
    UserRepository userRepository;

    public List<User> searchAll() {
        // ユーザーテーブルの全件検索
        return userRepository.findAll();
    }
}
```
・JPAでデータベース処理を行う際は、EntityManagerを使って行います。<br>　EntityManagerが持つメソッドには。

**検索:find**<br>
**登録:persist**<br>
**更新:merge**<br>
**削除:remove**<br><br>
等があります
<br><br>


### 5. htmlで表示
Thymeleafを使っているのでjspではなくhtmlで表示します<br>
src/main/resources/templates/の中にindex.htmlを作成してください
<br><br>index.html
```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
  <head>
    <title>Hello</title>
    <link href="/css/list.css" rel="stylesheet"></link>
    <meta charset="utf-8" />
  </head>
  <body>
    <h1>ユーザー情報一覧</h1>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>名前</th>

            </tr>
        </thead>
        <tbody>
            <tr th:each="user : ${userlist}" th:object="${user}">
                <td class="center" th:text="*{id}"></td>
                <td th:text="*{name}"></td>
            </tr>
        </tbody>
    </table>
  </body>
</html>
```
<br>
<br>
<br>
<br>

## プロジェクトの実行
SpringBootのライブラリーにTomcatが組み込まれているので<br>
サーバーの設定は必要ありません。<br><br>
プロジェクトを右クリック→実行→SpringBootアプリケーションを実行<br>
コンソールが下記のようになれば成功です
![Alt text](./photo/comp1.PNG)
<br>
http://localhost:8080/ に接続してみましょう
<br>
![Alt text](./photo/comp2.PNG)<br>
##### ユーザーの一覧が表示されます